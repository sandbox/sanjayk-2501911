INSTRUCTIONS
-------------
Download the module.
Download jquery.plugin.html2canvas.js javascript library
(div_screenshot/js/jquery.plugin.html2canvas.js).
Place it under sites/all/libraries/div_screenshot so that
the file can be found under
sites/all/libraries/div_screenshot/jquery.plugin.html2canvas.js.
Enable the module.

Note: for this module jQuery should be 1.7 or greater. If jquery 
version less than 1.7 download  jQuery update module from
https://www.drupal.org/project/jquery_update 

SUMMARY
-------
You can take a screenshot of a HTML element,
and capture all its content. To do this the 
id of the HTML element you want to take a 
picture of must be specified. For the 
example below we will select the div with the id
"div_screenshot" and output it as a png image.

<div id="div_screenshot">
	<table>
		<tr>
			<td>your content......</td>
			<td>images</td>
			<td>for element etc</td>
		</tr>
	</table>
</div>
