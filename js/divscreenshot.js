/**
 * @file
 * divscreenshot.js
 */

(function ($) {
  'use strict';
  Drupal.behaviors.div_screenshot = {
    attach: function (context, settings) {
      var basePath = Drupal.settings.basePath;

      // Defining urls for create screenshot and validate jquery version.
      var url = basePath + 'divscreenshot/';
      var check_jquery_ver_url = basePath + 'check_jquery_ver/';
      jQuery.post(check_jquery_ver_url, {jquery_version: jQuery().jquery});

      // Append submit button.
      jQuery('#div_screenshot').before("<input type='button' value='Capture' id='capture' /><div class='imageurl'></div>");
      jQuery('#capture').on('click', function (e) {
        jQuery('.imageurl').text('Loading....');
        e.preventDefault();
        jQuery('#div_screenshot').html2canvas({
          onrendered: function (canvas) {
            var canvasStr = canvas.toDataURL('image/png');
            canvasStr = canvasStr.replace('data:image/png;base64,', '');

            // Replace all forward slash with underscore.
            while (canvasStr.indexOf('/') !== -1) {
              canvasStr = canvasStr.replace('/', '_');
            }

            // Replace all plus with star.
            while (canvasStr.indexOf('+') !== -1) {
              canvasStr = canvasStr.replace('+', '*');
            }

            var posting = jQuery.post(url, {str: canvasStr});

            // Put the results in a div.
            posting.done(function (data) {
              jQuery('.imageurl').html(data);
            });
          }
        });
      });
    }
  };
})(jQuery);
